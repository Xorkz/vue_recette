import VueRouter from "vue-router";
import Vue from "vue";
import Login from "@/components/Login";
import Recipes from "@/components/Recipes";


Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        // Route login
        {
            path: "/",
            name: "Login",
            component: Login
        },
        // Route qui affiche l'homepage avec les recettes
        {
            path: "/allrecipe",
            name: "Recipes",
            component: Recipes
        }
    ]
});