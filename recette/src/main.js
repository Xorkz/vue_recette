import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import router from "./router/index.js";

// Ajout de la librairie MaterializeCSS
import "materialize-css/dist/js/materialize.min.js";
import "materialize-css/dist/css/materialize.min.css";

Vue.config.productionTip = false

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app')