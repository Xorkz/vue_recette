import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        jwt: "",
        user: []
    },

    actions: {
        // Requête axios sur le serveur pour la connexion
        login({ commit }, payload) {
            return axios.post("https://recette-api-server.herokuapp.com/login", payload).then((response) => {
                if (response.status == 200) {
                    commit("SET_JWT", response);
                }
                return response.data.jwt;
            });
        },

        // Requête axios sur le serveur pour l'inscription
        register({ commit }, payload) {
            return axios.post("https://recette-api-server.herokuapp.com/user", payload).then((response) => {
                    if (response.status == 200) {
                        commit("ADD_USER", response.data);
                    }
                    return response.data;
                })
                .then(response => {
                    console.log(response);
                })
                .catch(function(error) {
                    console.log(error.response)
                });
        }
    },

    mutations: {
        // Ajout du JWT dans le state
        SET_JWT(state, jwt) {
            state.jwt = jwt.data.jwt;
        },
        // Ajout du nouvel utilisateur dans le state
        ADD_USER(state, user) {
            state.user = user.data
        }
    }
});