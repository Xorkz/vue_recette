import axios from 'axios';

const state = {
    recipes: []
};

// Récupère les recettes venant du state
const getters = {
    allRecipes: (state) => {
        return state.recipes
    }
};

const actions = {
    // Requête axios sur le serveur pour récupérer toute les recettes
    async fetchRecipes({ commit }) {
        const response = await axios.get('https://recette-api-server.herokuapp.com/allRecipe');

        commit('setRecipes', response.data);
    },

    // Requête axios sur le serveur pour supprimer une recette
    async deleteRecipe({ commit }, obj) {

        var bearer = {
            "Authorization": "Bearer " + obj.jwt
        }

        const response = await axios.delete(`https://recette-api-server.herokuapp.com/recipe/${obj.id}`, { headers: bearer });

        commit('removeRecipe', response.data);
    },

    // Requête axios sur le serveur pour mettre à jour une recette
    async updateRecipe({ commit }, obj) {

        var bearer = {
            "Authorization": "Bearer " + obj.jwt
        }

        const response = await axios.patch(`https://recette-api-server.herokuapp.com/recipe/${obj.id}`, obj.payload, { headers: bearer });

        commit('updateRecipe', response.data);
    },

    // Requête axios sur le serveur pour ajouter une recette
    async addRecipe({ commit }, obj) {

        var bearer = {
            "Authorization": "Bearer " + obj.jwt
        }

        const response = await axios.post(`https://recette-api-server.herokuapp.com/recipe`, obj.payload, { headers: bearer })

        commit('addRecipe', response.data)
    },

}

const mutations = {
    // Ajout des recettes récupérer avec le fetch dans le state
    setRecipes: (state, recipes) => (state.recipes = recipes),
    // Suppression d'une recette dans le state
    removeRecipe: (state, id) => state.recipes = state.recipes.filter((recipe) => recipe.id !== id),
    // Mise à jour d'une recette dans le state
    updateRecipe: (state, updatedRecipe) => {
        // Find index
        const index = state.recipes.findIndex(recipe => recipe.id === updatedRecipe.id);

        if (index !== -1) {
            state.recipes.splice(index, 1, updatedRecipe);
        }
    },
    // Ajout d'une nouvelle recette dans le state
    addRecipe: (state, newRecipe) => state.recipes.unshift(newRecipe)
};


export default {
    state,
    getters,
    actions,
    mutations
};